#!/usr/bin/env bash

curl -X POST --data "{\"content\": \"$1\"}" \
 -H "Authorization: Bot $2" \
 -H "Content-Type: application/json" \
 https://discordapp.com/api/v6/channels/"$3"/messages
