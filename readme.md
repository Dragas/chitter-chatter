# Chitter Chatter

Simplifies your role playing needs when you want to talk as multiple bot users.

## Usage

First add a chatter:

```bash
./add_chatter.sh [token] [channelid] "[name]"
```

This generates a [name].sh file that you can use to post as that bot user. 
Then post as that user like so

```bash
./[name].sh "[message]"
```

## Troubleshooting

In case bash says that files are not executable just run the following:

```bash
chmod +x *.sh
```
