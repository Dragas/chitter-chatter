#!/usr/bin/env bash

token=$1
channelid=$2
name=$3

sed "s/\[token\]/$1/" chatter_template.sh | sed "s/\[channelid\]/$2/" | tee "$3.sh"
chmod +x "$3.sh"


